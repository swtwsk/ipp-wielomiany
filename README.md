# Wielomiany #

Projekt biblioteki obsługującej podstawowe operacje na wielomianach wielu zmiennych,
przygotowany na przedmiot IPP w roku 2017.

# Polynomials #

Project of library providing basic operations on multivariate polynomials,
made up for university class in 2017.