/** @file
   Kalkulator podstawowych operacji na wielomianach

   @author Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @date 2017-06-01
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

#include "poly.h"
#include "vector.h"
#include "print.h"

#include "utils.h"

/** Dynamiczna alokacja struktury przechowującej wielomiany */
#define STACKALLOC (Stack *) malloc(sizeof(Stack))

/** Struktura (stos) przechowująca wielomiany */
typedef struct Stack
{
	struct Poly *polymian; ///< wskaźnik na jednomian
	struct Stack *next; ///< wskaźnik na kolejny element stosu
} Stack;

/**
 * Tworzy nowy, pusty element stosu, leżący na górze.
 * @return pusty element stosu
 */
static Stack *CreateStackElement()
{
	Stack *new_stack_element = STACKALLOC;
	assert(new_stack_element != NULL);

	new_stack_element->next = NULL;
	new_stack_element->polymian = NULL;

	return new_stack_element;
}

/**
 * Umieszcza na szczycie stosu wielomian @p new_poly
 * @param[in] stack : stos
 * @param[in] new_poly : wielomian
 */
static void PushPolyOnStack(Stack **stack, Poly *new_poly)
{
	Stack *new_stack = CreateStackElement();
	new_stack->next = *stack;
	new_stack->polymian = POLYALLOC;
	*(new_stack->polymian) = *new_poly;
	*stack = new_stack;
}

/**
 * Umieszcza na stosie wielomian, który jest współczynnikiem.
 * Tworzy wielomian będący współczynnikiem i dodaje go na szczyt stosu.
 * @param[in] stack : stos
 * @param[in] coeff : wartość współczynnika
 */
static void PushNewPolyFromCoeff(Stack **stack, poly_coeff_t coeff)
{
	Stack *new_stack = CreateStackElement();
	new_stack->next = *stack;
	new_stack->polymian = POLYALLOC;
	*(new_stack->polymian) = PolyFromCoeff(coeff);

	*stack = new_stack;
}

/**
 * Usuwa element stosu leżący na szczycie.
 * @param[in] stack : stos
 */
static void PopFromStack(Stack **stack)
{
	Stack *next = (*stack)->next;

	PolyDestroy(&(*(*stack)->polymian));
	free((*stack)->polymian);
	free(*stack);

	*stack = next;
}

/**
 * Usuwa stos z pamięci.
 * Zwalnia pamięć zaalokowaną przez elementy stosu oraz trzymane w nich wielomiany.
 * @param[in] stack : stos
 */
static void FreeStack(Stack **stack)
{
	while (*stack != NULL)
	{
		PopFromStack(stack);
	}
}

//Predeklaracja funkcji ParsePoly
static Poly ParsePoly(char *next_char, int row_number, int *column_number, bool *is_error);

/**
 * Czyści bufor, sczytując standardowe wejście tak długo, aż pojawi się
 * znak końca linii.
 */
static void ClearBuffer()
{
	char c;

	do
	{
		if(scanf("%c", &c) != 1)
		{
			return;
		}
	} while (c != '\n');
}

/**
 * Wyświetla na wyjście diagnostyczne informację o wpisaniu złej komendy.
 * @param[in] row_number : numer wiersza
 */
static void PrintErrorCommand(int row_number)
{
	fprintf(stderr, "ERROR %d WRONG COMMAND\n", row_number);
	ClearBuffer();
}
/**
 * Wyświetla na wyjście diagnostyczne podaną treść błędu.
 * Funkcja czyści bufor, jeżeli jest to potrzebne.
 * @param[in] c : poprzednio wczytany znak
 * @param[in] row_number : numer wiersza
 * @param[in] message : treść błędu
 */
static void PrintErrorValue(char c, int row_number, char *message)
{
	fprintf(stderr, "ERROR %d %s\n", row_number, message);

	if (c != '\n')
	{
		ClearBuffer();
	}
}
/**
 * Wyświetla na wyjście diagnostyczne informację o za małej ilości wielomianów
 * na stosie.
 * @param[in] row_number : numer wiersza
 */
static void PrintErrorUnderflow(int row_number)
{
	fprintf(stderr, "ERROR %d STACK UNDERFLOW\n", row_number);
}
/**
 * Wyświetla na wyjście diagnostyczne informację o podaniu błędnego znaku.
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 */
static void PrintErrorByColumn(int row_number, int column_number)
{
	fprintf(stderr, "ERROR %d %d\n", row_number, column_number);
}
/**
 * Wyświetla na wyjście diagnostyczne informację o podaniu błędnego znaku,
 * jeżeli błąd dopiero się pojawił.
 * Przekazuje wtedy - poprzez @p is_error - informację o wyświetleniu błędu
 * i o samym błędzie.
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 */
static void PrintErrorByColumnAndCheck(bool *is_error, int row_number, int column_number)
{
	if (!(*is_error))
	{
		PrintErrorByColumn(row_number, column_number);
		*is_error = true;
	}
}

/**
 * Sczytuje znak i sprawdza, czy jest to znak końca linii.
 * @param[in] row_number : numer wiersza
 * @return Czy sczytany znak jest znakiem końca linii?
 */
static bool CheckLastSymbol(int row_number)
{
	char last_letter;
	scanf("%c", &last_letter);

	if (last_letter != '\n')
	{
		PrintErrorCommand(row_number);
		return false;
	}

	return true;
}

/**
 * Sczytuje ze standardowego wejścia liczbę typu @p poly_exp_t - wykładnik wielomianu.
 * Jeżeli liczba przekroczy zadany zakres, przekazuje informację o błędzie.
 * @param[in] next_char : wczytany przed wywołaniem funkcji znak
 * @param[in] column_number : numer kolumny
 * @param[in] max_val : maksymalna wielkość liczby
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 * @return wczytana liczba
 */
poly_exp_t ParseExpNumber(char *next_char, int *column_number, unsigned max_val, bool *is_error)
{
	poly_exp_t result = 0;

	do
	{
		if (!isdigit(*next_char))
		{
			return result;
		}
		else
		{
			if ((unsigned int)result > max_val / 10 || ((unsigned int)result == max_val / 10 && (unsigned int)(*next_char - '0') > max_val % 10))
			{
				*is_error = true;
				return result;
			}

			result = result * 10 + (*next_char - '0');
			(*column_number)++;
		}
	} while (scanf("%c", next_char) == 1);

	return result;
}
/**
 * Sczytuje ze standardowego wejścia liczbę typu @p poly_coeff_t - współczynnik wielomianu.
 * Jeżeli liczba przekroczy zakres LONG_MAX/LONG_MIN, przekazuje informację o błędzie.
 * @param[in] next_char : wczytany przed wywołaniem funkcji znak
 * @param[in] column_number : numer kolumny
 * @param[in] is_negative : czy liczba przyjmie wartość ujemną?
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 * @return wczytana liczba
 */
poly_coeff_t ParseCoeffNumber(char *next_char, int *column_number, bool is_negative, bool *is_error)
{
	poly_coeff_t result = 0;

	do
	{
		if (!isdigit(*next_char))
		{
			return result;
		}
		else
		{
			if (!is_negative)
			{
				if (result > LONG_MAX / 10 || (result == LONG_MAX / 10 && (*next_char - '0') > LONG_MAX % 10))
				{
					*is_error = true;
					return result;
				}
			}
			else
			{
				if (result > -(LONG_MIN / 10) || (result == -(LONG_MIN / 10) && (*next_char - '0') > -(LONG_MIN % 10)))
				{
					*is_error = true;
					return result;
				}
			}

			result = result * 10 + (*next_char - '0');
			(*column_number)++;
		}
	} while (scanf("%c", next_char) == 1);

	return result;
}
/**
* Sczytuje ze standardowego wejścia liczbę typu @p unsigned
* Jeżeli liczba przekroczy zakres UINT_MAX, przekazuje informację o błędzie.
* @param[in] next_char : wczytany przed wywołaniem funkcji znak
* @param[in] column_number : numer kolumny
* @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
* @return wczytana liczba
*/
unsigned ParseCountNumber(char *next_char, int *column_number, bool *is_error)
{
	unsigned result = 0;

	do
	{
		if (!isdigit(*next_char))
		{
			return result;
		}
		else
		{
			if (result > UINT_MAX / 10 || (result == UINT_MAX / 10 && (unsigned)(*next_char - '0') > UINT_MAX % 10))
			{
				*is_error = true;
				return result;
			}

			result = result * 10 + (*next_char - '0');
			(*column_number)++;
		}
	} while (scanf("%c", next_char) == 1);

	return result;
}

/**
 * Wyświetla wynik operacji logicznej dla wielomianu umieszczonego na stosie.
 * Wyświetla na standardowe wyjście liczbę 1 albo 0.
 * Przyjmuje jako argument wskaźnik na funkcję zwracającą wartość logiczną dla
 * zadanego wielomianu.
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : stos wielomianów
 * @param[in] row_number : numer wiersza
 * @param[in] op : funkcja
 */
static void OperationBool(Stack *stack, int row_number, bool(*op)(const Poly*))
{
	if (CheckLastSymbol(row_number))
	{
		if (stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			if (op(&(*stack->polymian)))
			{
				printf("1\n");
			}
			else
			{
				printf("0\n");
			}
		}
	}
}
/**
 * Przeprowadza zadaną operację dla wielomianu umieszczonego na stosie.
 * Przyjmuje jako argument wskaźnik na funkcję przyjmującą jako argument wielomian.
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : wskaźnik na stos wielomianów
 * @param[in] row_number : numer wiersza
 * @param[in] op : funkcja
 */
static void OperationOneArg(Stack **stack, int row_number, Poly(*op)(const Poly *))
{
	if (CheckLastSymbol(row_number))
	{
		if (*stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			Stack *new_stack = CreateStackElement();
			new_stack->next = *stack;
			new_stack->polymian = POLYALLOC;
			*(new_stack->polymian) = op(&(*(*stack)->polymian));
			*stack = new_stack;
		}
	}
}
/**
 * Przeprowadza zadaną operację dla dwóch wielomianów umieszczonych na stosie.
 * Przyjmuje jako argument wskaźnik na funkcję przyjmującą jako argumenty
 * dwa wielomiany.
 * Gdy na stosie jest zbyt mało wielomianów, wyświetla błąd na wyjście
 * diagnostyczne.
 * @param[in] stack : wskaźnik na stos wielomianów
 * @param[in] row_number : numer wiersza
 * @param[in] op : funkcja
 */
static void OperationTwoArg(Stack **stack, int row_number, Poly(*op)(const Poly *, const Poly *))
{
	if (CheckLastSymbol(row_number))
	{
		if (*stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else if ((*stack)->next == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			Stack *new_stack = CreateStackElement();
			new_stack->next = (*stack)->next->next;
			new_stack->polymian = POLYALLOC;
			*(new_stack->polymian) = op(&(*(*stack)->polymian), &(*(*stack)->next->polymian));

			PolyDestroy(&(*(*stack)->polymian));
			PolyDestroy(&(*(*stack)->next->polymian));

			free((*stack)->next->polymian);
			free((*stack)->next);
			free((*stack)->polymian);
			free(*stack);

			*stack = new_stack;
		}
	}
}

/**
 * Sprawdza, czy dwa umieszczone na stosie wielomiany są sobie równe.
 * Gdy na stosie jest zbyt mało wielomianów, wyświetla błąd na wyjście
 * diagnostyczne.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void IsEq(Stack *stack, int row_number)
{
	if (CheckLastSymbol(row_number))
	{
		if (stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else if (stack->next == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			if (PolyIsEq(&(*stack->polymian), &(*stack->next->polymian)))
			{
				printf("1\n");
			}
			else
			{
				printf("0\n");
			}
		}
	}
}

/**
 * Umieszcza na stosie wielomian tożsamościowo równy zeru.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void Zero(Stack **stack, int row_number)
{
	if (CheckLastSymbol(row_number))
	{
		Stack *new_stack = CreateStackElement();
		new_stack->next = *stack;
		new_stack->polymian = POLYALLOC;
		*(new_stack->polymian) = PolyZero();
		*stack = new_stack;
	}
}

/**
 * Wyświetla stopień wielomianu na szczycie stosu (-1 dla wielomianu
 * tożsamościowo równego zeru).
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void Deg(Stack *stack, int row_number)
{
	if (CheckLastSymbol(row_number))
	{
		if (stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			printf("%d\n", PolyDeg(&(*stack->polymian)));
		}
	}
}

/**
 * Sprawdza, czy wczytany po komendzie znak jest spacją.
 * W przeciwnym przypadku wyświetla błąd na wyjście diagnostyczne.
 * @param[in] row_number : numer wiersza
 * @param[in] error_message : treść błędu
 * @return Czy wczytany znak jest spacją?
 */
static bool CheckIfSpace(int row_number, char *error_message)
{
	char c;
	scanf("%c", &c);

	if (c != ' ')
	{
		PrintErrorValue(c, row_number, error_message);
		return false;
	}

	return true;
}

/**
 * Wyświetla stopień wielomianu na szczycie stosu (-1 dla wielomianu
 * tożsamościowo równego zeru) ze względu na zadaną zmienną.
 * Wczytuje zmienną. W przypadku podania błędnej zmiennej wyświetla
 * na wyjściu diagnostycznym stosowną informację.
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void DegBy(Stack *stack, int row_number)
{
	poly_exp_t idx;
	char c;
	int column_number = 7;
	char error_message[] = "WRONG VARIABLE";

	if (!CheckIfSpace(row_number, error_message))
	{
		return;
	}

	scanf("%c", &c);
	if (!isdigit(c))
	{
		PrintErrorValue(c, row_number, error_message);
	}
	else
	{
		bool is_error = false;
		idx = ParseExpNumber(&c, &column_number, UINT_MAX, &is_error);

		if (c != '\n' || is_error)
		{
			PrintErrorValue(c, row_number, error_message);
		}
		else
		{
			if (stack == NULL)
			{
				PrintErrorUnderflow(row_number);
			}
			else
			{
				printf("%d\n", PolyDegBy(stack->polymian, idx));
			}
		}
	}
}

/**
 * Umieszcza na szczycie stosu wielomian utworzony poprzez wstawienie wartości
 * @p x pod zmienną główną wielomianu leżącego na stosie.
 * @param[in] stack : stos
 * @param[in] x : wartość
 */
static void PushPolyAtOnStack(Stack **stack, poly_coeff_t x)
{
	Poly *poly_at = POLYALLOC;
	*poly_at = PolyAt((*stack)->polymian, x);
	PolyDestroy((*stack)->polymian);
	(*stack)->polymian = poly_at;
}
/**
 * Wczytuje wartość @p x i wylicza wartość wielomianu w tym punkcie.
 * Umieszcza na szczycie stosu wielomian utworzony poprzez wstawienie wartości
 * @p x pod zmienną główną wielomianu leżącego na stosie.
 * W przypadku, gdy podana zostaje błędna zmienna lub stos jest pusty, wyświetla
 * na wyjściu diagnostycznym stosowną informację.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void At(Stack **stack, int row_number)
{
	poly_coeff_t x;
	char c;
	int column_number = 3;
	bool is_error = false;
	char error_message[] = "WRONG VALUE";

	if (!CheckIfSpace(row_number, error_message))
	{
		return;
	}

	scanf("%c", &c);
	if (!isdigit(c))
	{
		if (c != '-')
		{
			PrintErrorValue(c, row_number, error_message);
			return;
		}
		else
		{
			scanf("%c", &c);
			if (!isdigit(c))
			{
				PrintErrorValue(c, row_number, error_message);
				return;
			}
			x = ParseCoeffNumber(&c, &column_number, true, &is_error);
		}
	}
	else
	{
		x = ParseCoeffNumber(&c, &column_number, false, &is_error);
	}

	if (c != '\n' || is_error)
	{
		PrintErrorValue(c, row_number, error_message);
	}
	else
	{
		if (*stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			PushPolyAtOnStack(stack, x);
		}
	}
}

/**
 * Iteruje poprzez stos, usuwając kolejne elementy stosu.
 * Nie wykonuje @p PolyDestroy na trzymanym na stosie wielomianie.
 * @param[in] stack : stos
 */
static void IterateStack(Stack **stack)
{
	Stack *next = (*stack)->next;
	free((*stack)->polymian);
	free(*stack);
	*stack = next;
}
/**
 * Sprawdza, czy na stosie znajduje się odpowiednia ilość wielomianów, by
 * wykonać PolyCompose. W przeciwnym przypadku zwraca fałsz.
 * @param[in] stack : stos
 * @param[in] count : ilość wielomianów
 * @return Czy jest odpowiednia ilość wielomianów?
 */
static bool CountCompose(Stack *stack, unsigned count)
{
	Stack *stack_iterator = stack;

	for (unsigned i = 0; i <= count; i++)
	{
		if (stack_iterator == NULL)
		{
			return false;
		}
		stack_iterator = stack_iterator->next;
	}

	return true;
}
/**
 * Tworzy dynamiczną tablicę wielomianów z elementów leżących na stosie.
 * @param[in] stack : stos
 * @param[in] count : ilość wielomianów
 * @return tablica wielomianów
 */
static Poly *PolyTabFromStack(Stack **stack, unsigned count)
{
	Poly *new_elements = (Poly*)malloc(sizeof(Poly) * count);

	for (unsigned i = 0; i < count; i++)
	{
		new_elements[i] = *(*stack)->polymian;

		IterateStack(stack);
	}

	return new_elements;
}
/**
 * Iteruje po tablicy wielomianów i usuwa je z pamięci za pomocą funkcji PolyDestroy.
 * @param[in] tab : tablica wielomianów
 * @param[in] count : ilość wielomianów
 */
static void PolyTabClear(Poly tab[], unsigned count)
{
	for (unsigned i = 0; i < count; i++)
	{
		PolyDestroy(&tab[i]);
	}
}
/**
 * Umieszcza na stosie wielomian stworzony z podstawiania leżących na stosie
 * wielomianów pod kolejne zmienne wielomianu leżącego na szczycie stosu.
 * @param[in] stack : stos
 * @param[in] count : ilość wielomianów
 */
static void PushPolyComposeOnStack(Stack **stack, unsigned count)
{
	Poly to_compose = *(*stack)->polymian;

	IterateStack(stack);

	Poly *tab = PolyTabFromStack(stack, count);

	Stack *new_stack = CreateStackElement();
	new_stack->next = *stack;
	new_stack->polymian = POLYALLOC;
	*(new_stack->polymian) = PolyCompose(&to_compose, count, tab);

	PolyTabClear(tab, count);
	free(tab);

	PolyDestroy(&to_compose);

	*stack = new_stack;
}
/**
 * Umieszcza na stosie wielomian stworzony z podstawiania leżących na stosie
 * wielomianów pod kolejne zmienne wielomianu leżącego na szczycie stosu.
 * Wczytuje ze standardowego wejścia ilość wielomianów do podstawienia.
 * Jeżeli na stosie jest zbyt mało wielomianów bądź została podana zła liczba,
 * zwraca na wyjście diagnostyczne odpowiedni błąd.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void Compose(Stack **stack, int row_number)
{
	unsigned count;
	char c;
	int column_number = 3;
	bool is_error = false;
	char count_error_message[] = "WRONG COUNT";

	if (!CheckIfSpace(row_number, count_error_message))
	{
		return;
	}

	scanf("%c", &c);
	if (!isdigit(c))
	{
		PrintErrorValue(c, row_number, count_error_message);
		return;
	}
	else
	{
		count = ParseCountNumber(&c, &column_number, &is_error);
		if (count == 0)
		{
			PrintErrorValue(c, row_number, count_error_message);
			return;
		}
	}

	if (c != '\n' || is_error)
	{
		PrintErrorValue(c, row_number, count_error_message);
	}
	else
	{
		if (!CountCompose(*stack, count))
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			PushPolyComposeOnStack(stack, count);
		}
	}
}

/**
 * Wyświetla wielomian leżący na szczycie stosu.
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void Print(Stack *stack, int row_number)
{
	if (CheckLastSymbol(row_number))
	{
		if (stack == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			PolyPrint(stack->polymian);
			printf("\n");
		}
	}
}

/**
 * Usuwa wielomian leżący na szczycie stosu.
 * Gdy stos jest pusty, wyświetla błąd na wyjście diagnostyczne.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void Pop(Stack **stack, int row_number)
{
	if (CheckLastSymbol(row_number))
	{
		if ((*stack) == NULL)
		{
			PrintErrorUnderflow(row_number);
		}
		else
		{
			PopFromStack(stack);
		}
	}
}

/**
 * Obsługuje błąd podczas tworzenia jednomianu @p m
 * Wyświetla błąd na wyjście diagnostyczne, o ile nie zostało to jeszcze zrobione.
 * Usuwa zaalokowaną przez wielomian będący współczynnikiem pamięć i zwraca
 * monomian postaci `0 * x^0`
 * @param[in] m : jednomian
 * @param[in] p : wielomian
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 */
static void HandleMonoError(Mono *m, Poly *p, int row_number, int column_number, bool *is_error)
{
	PrintErrorByColumnAndCheck(is_error, row_number, column_number);

	PolyDestroy(p);

	*p = PolyZero();
	*m = MonoFromPoly(p, 0);
}
/**
 * Parsuje wielomian będący współczynnikiem jednomianu @p new_mono
 * W przypadku błędu wyświetla informację na wyjście diagnostyczne
 * @param[in] new_poly : tworzony wielomian
 * @param[in] new_mono : tworzony jednomian
 * @param[in] next_char : znak wczytany przed wywołaniem
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 * @return Czy udało się sczytać współczynnik jednomianu?
 */
static bool ParseMonoCoeff(Poly *new_poly, Mono *new_mono, char *next_char,
	int row_number, int *column_number, bool *is_error)
{
	if (isdigit(*next_char))
	{
		*new_poly = PolyFromCoeff(ParseCoeffNumber(next_char, column_number, false, is_error));
		if (*is_error)
		{
			PrintErrorByColumn(row_number, *column_number);

			HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
			return false;
		}
	}
	else if (*next_char == '(')
	{
		*new_poly = ParsePoly(next_char, row_number, column_number, is_error);
	}
	else if (*next_char == '-')
	{
		scanf("%c", next_char);
		(*column_number)++;

		if (!isdigit(*next_char))
		{
			HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
			return false;
		}

		*new_poly = PolyFromCoeff((-1) * ParseCoeffNumber(next_char, column_number, true, is_error));
		if (*is_error)
		{
			PrintErrorByColumn(row_number, *column_number);

			HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
			return false;
		}
	}

	return true;
}
/**
 * Parsuje wykładnik jednomianu @p new_mono, którego współczynnikiem jest @p new_poly
 * W przypadku błędu wyświetla informację na wyjście diagnostyczne
 * @param[in] new_poly : wielomian
 * @param[in] new_mono : tworzony jednomian
 * @param[in] next_char : znak wczytany przed wywołaniem
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 */
static void ParseMonoExp(Poly *new_poly, Mono *new_mono, char *next_char, int row_number, int *column_number,
	bool *is_error)
{
	scanf("%c", next_char);
	if (!isdigit(*next_char))
	{
		(*column_number)++;

		HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
		return;
	}

	*new_mono = MonoFromPoly(new_poly, ParseExpNumber(next_char, column_number, INT_MAX, is_error));

	if (*is_error)
	{
		PrintErrorByColumn(row_number, *column_number);

		HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
		return;
	}

	if (*next_char != ')')
	{
		(*column_number)++;

		HandleMonoError(new_mono, new_poly, row_number, *column_number, is_error);
		return;
	}
}
/**
 * Sczytuje ze standardowego wejścia jednomian i go zwraca.
 * W przypadku błędu wyświetla informację na wyjście diagnostyczne
 * @param[in] next_char : znak wczytany przed wywołaniem funkcji
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 * @return jednomian
 */
static Mono ParseNewMono(char *next_char, int row_number, int *column_number, bool *is_error)
{
	Poly new_poly = PolyZero();
	Mono new_mono;

	if (!ParseMonoCoeff(&new_poly, &new_mono, next_char, row_number, column_number, is_error))
	{
		return new_mono;
	}

	if (*next_char != ',')
	{
		HandleMonoError(&new_mono, &new_poly, row_number, *column_number, is_error);
		return new_mono;
	}

	ParseMonoExp(&new_poly, &new_mono, next_char, row_number, column_number, is_error);

	return new_mono;
}

/**
 * Sczytuje ze standardowego wejścia jednomian i umieszcza w kontenerze.
 * Kontener @p v zawiera jednomiany, które zostaną zsumowane do wielomianu.
 * Jeżeli któryś z jednomianów zostanie błędnie wczytany, cały kontener
 * zostanie usunięty, a informacja o błędzie przekazana dalej.
 * @param[in] v : kontener
 * @param[in] next_char : znak wczytany przed wywołaniem
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 */
static bool PutMonoOnVector(Vector **v, char *next_char, int row_number,
	int *column_number, bool *is_error)
{
	Mono *new_mono = MONOALLOC;
	*new_mono = ParseNewMono(next_char, row_number, column_number, is_error);
	VectorAdd(v, new_mono);
	free(new_mono);

	if (*is_error)
	{
		VectorDelete(v);
		return false;
	}

	return true;
}
/**
 * Sczytuje ze standardowego wejścia wielomian i umieszcza go na stosie.
 * Jeżeli któryś z jednomianów zostanie błędnie wczytany, wielomian nie zostaje
 * umieszczony.
 * @param[in] next_char : znak wczytany przed wywołaniem
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 * @param[in] is_error : wskaźnik na zmienną logiczną obsługującą błąd
 */
static Poly ParsePoly(char *next_char, int row_number, int *column_number, bool *is_error)
{
	Vector *vector = CreateNewVector();
	int bracket_new_amount = 1;
	Poly new_poly = PolyZero();

	while (scanf("%c", next_char) == 1)
	{
		(*column_number)++;

		if (*next_char == '(')
		{
			bracket_new_amount++;

			if (bracket_new_amount > 1)
			{
				if (!PutMonoOnVector(&vector, next_char, row_number, column_number, is_error))
				{
					return new_poly;
				}

				bracket_new_amount -= 2;
			}
		}
		else if (*next_char == '-' || isdigit(*next_char))
		{
			if (!PutMonoOnVector(&vector, next_char, row_number, column_number, is_error))
			{
				return new_poly;
			}

			bracket_new_amount--;
		}
		else if (*next_char != '+')
		{
			if ((*next_char == ',' || *next_char == '\n') && VectorActiveSize(vector) > 0)
			{
				new_poly = PolyAddMonos(VectorActiveSize(vector), VectorGetMonoTab(vector));
			}
			else
			{
				PrintErrorByColumnAndCheck(is_error, row_number, *column_number);
			}

			VectorDelete(&vector);
			return new_poly;
		}
	}

	return new_poly;
}

/**
 * Sczytuje komendę ze standardowego wejścia.
 * Komenda rozpoczyna się wielką lub małą literą.
 * @param[in] stack : stos
 * @param[in] row_number : numer wiersza
 */
static void ParseFromText(Stack **stack, int row_number)
{
	char command[9];
	scanf("%8[^ 0-9\n]", command);

	if (strcmp(command, "ZERO") == 0)
	{
		Zero(stack, row_number);
	}
	else if (strcmp(command, "IS_COEFF") == 0)
	{
		OperationBool(*stack, row_number, PolyIsCoeff);
	}
	else if (strcmp(command, "IS_ZERO") == 0)
	{
		OperationBool(*stack, row_number, PolyIsZero);
	}
	else if (strcmp(command, "CLONE") == 0)
	{
		OperationOneArg(stack, row_number, PolyClone);
	}
	else if (strcmp(command, "ADD") == 0)
	{
		OperationTwoArg(stack, row_number, PolyAdd);
	}
	else if (strcmp(command, "MUL") == 0)
	{
		OperationTwoArg(stack, row_number, PolyMul);
	}
	else if (strcmp(command, "NEG") == 0)
	{
		OperationOneArg(stack, row_number, PolyNeg);
	}
	else if (strcmp(command, "SUB") == 0)
	{
		OperationTwoArg(stack, row_number, PolySub);
	}
	else if (strcmp(command, "IS_EQ") == 0)
	{
		IsEq(*stack, row_number);
	}
	else if (strcmp(command, "DEG") == 0)
	{
		Deg(*stack, row_number);
	}
	else if (strcmp(command, "PRINT") == 0)
	{
		Print(*stack, row_number);
	}
	else if (strcmp(command, "POP") == 0)
	{
		Pop(stack, row_number);
	}
	else if (strcmp(command, "DEG_BY") == 0)
	{
		DegBy(*stack, row_number);
	}
	else if (strcmp(command, "AT") == 0)
	{
		At(stack, row_number);
	}
	else if (strcmp(command, "COMPOSE") == 0)
	{
		Compose(stack, row_number);
	}
	else
	{
		PrintErrorCommand(row_number);
	}
}

/**
 * Umieszcza na stosie wielomian będący współczynnikiem.
 * W przypadku błędu usuwa ze stosu dopiero co umieszczony tam wielomian.
 * @param[in] stack : stos
 * @param[in] c : znak wczytany wcześniej
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 */
static void IsDigit(Stack **stack, char *c, int row_number, int *column_number)
{
	bool is_error = false;

	PushNewPolyFromCoeff(stack, ParseCoeffNumber(c, column_number, false, &is_error));

	if (*c != '\n' || is_error)
	{
		PrintErrorByColumn(row_number, *column_number);
		Pop(stack, row_number);
	}
}
/**
 * Umieszcza na stosie wielomian będący sumą jednomianów.
 * Nie robi tego w przypadku błędu.
 * @param[in] stack : stos
 * @param[in] c : znak wczytany wcześniej
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 */
static void IsBracket(Stack **stack, char *c, int row_number, int *column_number)
{
	bool is_error = false;

	Poly new_poly = ParsePoly(c, row_number, column_number, &is_error);

	if (!is_error)
	{
		PushPolyOnStack(stack, &new_poly);
	}
	else
	{
		PolyDestroy(&new_poly);
		ClearBuffer();
	}
}
/**
 * Umieszcza na stosie wielomian będący współczynnikiem o ujemnym znaku.
 * Nie robi tego w przypadku błędu.
 * @param[in] stack : stos
 * @param[in] c : znak wczytany wcześniej
 * @param[in] row_number : numer wiersza
 * @param[in] column_number : numer kolumny
 */
static void IsSign(Stack **stack, char *c, int row_number, int *column_number)
{
	scanf("%c", c);

	if (isdigit(*c))
	{
		bool is_error = false;

		PushNewPolyFromCoeff(stack, (-1) * ParseCoeffNumber(c, column_number, true, &is_error));

		if (*c != '\n' || is_error)
		{
			PrintErrorByColumn(row_number, *column_number);
			Pop(stack, row_number);
		}
	}
	else
	{
		PrintErrorByColumn(row_number, *column_number);
	}
}
/**
 * Główna funkcja programu kalkulatora.
 * Kalkulator umożliwia wczytywanie wielomianów i przeprowadzanie na nich
 * podstawowych operacji.
 */
int main()
{
	char c;
	int row_number = 1;
	Stack *stack = NULL;

	while (scanf("%c", &c) == 1)
	{
		int column_number = 1;

		if (isalpha(c))
		{
			ungetc(c, stdin);
			ParseFromText(&stack, row_number);
		}
		else if (isdigit(c))
		{
			IsDigit(&stack, &c, row_number, &column_number);
		}
		else if (c == '(')
		{
			IsBracket(&stack, &c, row_number, &column_number);
		}
		else if (c == '-')
		{
			IsSign(&stack, &c, row_number, &column_number);
		}
		else if (c != '\n')
		{
			PrintErrorByColumn(row_number, column_number);
			ClearBuffer();
		}

		row_number++;
	}

	FreeStack(&stack);

	return 0;
}