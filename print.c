/** @file
   Obsługa wyświetlania wielomianów

   @author Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @date 2017-05-24
*/

#include "print.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/** Dynamiczna alokacja stosu przechowującego jednomiany. */
#define MONOSTACKALLOC (MonoStack *) malloc(sizeof(MonoStack))

/** Struktura przechowująca stos jednomianów */
typedef struct MonoStack
{
	struct Mono *pointer; ///< wskaźnik na jednomian
	struct MonoStack *next; ///< wskaźnik na kolejny element stosu jednomianów
} MonoStack;

/**
 * Tworzy nowy, pusty element stosu, leżący na górze.
 * @return pusty element stosu
 */
static MonoStack *CreateMonoStackElement()
{
	MonoStack *new_stack_element = MONOSTACKALLOC;
	assert(new_stack_element != NULL);

	new_stack_element->pointer = NULL;
	new_stack_element->next = NULL;

	return new_stack_element;
}

/**
 * Umieszcza jednomian @p new_mono na szczycie stosu.
 * @param[in] stack : wskaźnik na stos
 * @param[in] new_mono : jednomian
 */
static void PushOnMonoStack(MonoStack **stack, Mono *new_mono)
{
	MonoStack *new_stack = CreateMonoStackElement();
	new_stack->next = *stack;
	new_stack->pointer = new_mono;

	*stack = new_stack;
}

/**
 * Wyświetla wielomian @p poly_to_print na standardowe wyjście.
 * Wielomian uporządkowany jest w kolejności rosnącej względem wielkości
 * wykładnika.
 * @param[in] poly_to_print : wielomian
 */
void PolyPrint(Poly *poly_to_print)
{
	MonoList *list_iterator = ReturnMonoListStart(poly_to_print);
	MonoStack *stack = NULL;

	bool printed = false;
	poly_coeff_t constant = ReturnPolyCoeff(poly_to_print);

	if (PolyIsCoeff(poly_to_print))
	{
		printf("%ld", constant);
	}
	else
	{
		if (constant != 0)
		{
			printf("(%ld,0)", constant);
			printed = true;
		}

		while (!MonoListIsEmpty(list_iterator))
		{
			PushOnMonoStack(&stack, ReturnMono(list_iterator));
			IterateMonoList(&list_iterator);
		}

		while (stack != NULL)
		{
			if (printed)
			{
				printf("+");
			}
			
			printed = true;
			printf("(");
			PolyPrint(&stack->pointer->p);
			printf(",%d)", stack->pointer->exp);

			MonoStack *next_on_stack = stack->next;
			free(stack);
			stack = next_on_stack;
		}
	}
}