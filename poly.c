/** @file
   Interfejs klasy wielomianów

   @author Jakub Pawlewicz <pan@mimuw.edu.pl>, Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-06-01
*/

#include "poly.h"
#include <stdlib.h>
#include <assert.h>

/**
 * Zwraca większą z dwóch wartości.
 * @param[in] a : wartość
 * @param[in] b : wartość
 * @return maksimum z a i b
 */
#define MAX(a,b) ((a) > (b) ? a : b)
/**
 * Ujemna stała, która służy do wykrywania jednomianów postaci
 * (wielomian innej zmiennej) * x^0 podczas dodawania lub mnożenia
 * albo do oznaczania wielomianów przeznaczonych do usunięcia podczas
 * sumowania listy wielomianów
 */
#define NEGATIVE_TEN -10

/** Struktura przechowująca listę jednomianów. */
struct MonoList
{
	struct Mono *pointer; ///< wskaźnik na jednomian
	struct MonoList *next; ///< wskaźnik na kolejny element listy
};

/** 
 * Zwraca wykładnik jednomianu dla zmiennej głównej tego jednomianu.
 * Zwraca stałą @p NEGATIVE_TEN, gdy jest to jednomian postaci ( wielomian innej zmiennej) * x^0.
 * @param[in] list_pointer : wskaźnik na element listy przechowującej jednomiany
 * @return wykładnik jednomianu przechowywanego w @p list_pointer lub @p NEGATIVE_TEN
 */
static inline poly_exp_t MonoExpIsZero(const MonoList *list_pointer)
{
	if (list_pointer->pointer->exp == 0) //mamy wielomian postaci ( wielomian zmiennej y ) * x^0
	{
		return NEGATIVE_TEN;
	}
	else
	{
		return list_pointer->pointer->exp;
	}
}
/** 
 * Zwraca stopień wielomianu dla zmiennej głównej tego wielomianu (-1 dla wielomianu
 * tożsamościowo równego zeru lub @p NEGATIVE_TEN dla wielomianu postaci
 * ( wielomian innej zmiennej ) * x^0).
 * @param[in] p : wielomian
 * @return stopień wielomianu @p p dla zmiennej głównej wielomianu
 */
static poly_exp_t MainPolyDeg(const Poly *p)
{
	if (PolyIsZero(p))
	{
		return (-1);
	}
	else if (PolyIsCoeff(p))
	{
		return 0;
	}
	else
	{
		return MonoExpIsZero(p->start);
	}
}
/**
 * Sprawdza, czy element listy nie jest jej końcem (zwracając 0);
 * jeżeli nie, to zwraca wykładnik jednomianu przechowywanego na tym elemencie listy.
 * @param[in] list_pointer : wskaźnik na element listy przechowującej jednomiany
 * @return 0, jeżeli jest to koniec listy lub stopień jednomianu w przeciwnym przypadku
 */
static inline poly_exp_t ListPointerIsNull(const MonoList *list_pointer)
{
	if (list_pointer != NULL)
	{
		return MonoExpIsZero(list_pointer);
	}
	else
	{
		return 0;
	}
}
/**
 * Tworzy nowy, pusty element listy jednomianów.
 * @return pusty element listy
 */
static MonoList *CreateListNode()
{
	MonoList *new_list = LISTALLOC;
	assert(new_list != NULL);

	new_list->next = NULL;
	new_list->pointer = NULL;

	return new_list;
}

/**
 * Usuwa wielomian z pamięci.
 * Tak długo, jak wielomian nie jest współczynnikiem funkcja usuwa kolejne
 * elementy listy i rekurencyjnie trzymane na niej jednomiany.
 * @param[in] p : wielomian
 */
void PolyDestroy(Poly *p)
{
	while (!PolyIsCoeff(p))
	{
		MonoList *next = p->start->next;
		MonoDestroy(p->start->pointer);
		free(p->start->pointer); //usuń jednomian
		free(p->start); //usuń element listy
		p->start = next;
	}
}

/**
 * Wykonuje pełną, głęboką kopię jednomianu do pamięci dynamicznej.
 * @param[in] m : jednomian
 * @return wskaźnik na jednomian zaalokowany w pamięci dynamicznej
 */
static Mono *DynamicMonoClone(const Mono *m)
{
	Mono *new_mono = MONOALLOC;
	assert(new_mono != NULL);

	new_mono->exp = m->exp;
	new_mono->p = PolyClone(&m->p);

	return new_mono;
}
/**
 * Robi pełną, głęboką kopię wielomianu.
 * @param[in] p : wielomian
 * @return skopiowany wielomian
 */
Poly PolyClone(const Poly *p)
{
	Poly new_poly = PolyFromCoeff(p->c);

	if (!PolyIsCoeff(p))
	{
		MonoList *new_list = CreateListNode();
		new_poly.start = new_list;

		MonoList *list_iterator = p->start;
		while (list_iterator != NULL)
		{
			new_list->pointer = DynamicMonoClone(list_iterator->pointer);

			list_iterator = list_iterator->next;
			if (list_iterator != NULL)
			{
				new_list->next = CreateListNode();
				new_list = new_list->next;
			}
		}
	}

	return new_poly;
}

/**
 * Usuwa element listy jednomianów wraz z trzymanym na nim jednomianem.
 * Rekurencyjnie usuwa jednomian z pamięci dynamicznej.
 * @param[in] element_to_delete : element listy
 */
static void MonoListPop(MonoList **element_to_delete)
{
	MonoDestroy((*element_to_delete)->pointer);
	free((*element_to_delete)->pointer);
	free(*element_to_delete);
}
/**
 * Usuwa z początku wielomianu jednomiany o współczynniku tożsamościowo równym zero.
 * Usuwa po kolejnych najwyższych potęgach tak długo, aż trafi na jednomian
 * o współczynniku różnym od zera, przesuwając równocześnie wskaźnik na początek listy.
 * @param[in] new_poly : wielomian
 */
static void FrontRemoving(Poly *new_poly)
{
	MonoList *next; ///< pomocniczy wskaźnik na kolejny element listy

	while (new_poly->start != NULL)
	{
		if (PolyIsCoeff(&new_poly->start->pointer->p))
		{
			if (new_poly->start->pointer->p.c == 0)
			{
				next = new_poly->start->next;
				MonoListPop(&new_poly->start);
				new_poly->start = next;
			}
			else if (new_poly->start->pointer->exp == 0)
			{
				new_poly->c = new_poly->start->pointer->p.c;

				next = new_poly->start->next;
				MonoListPop(&new_poly->start);
				new_poly->start = next;
			}
			else
			{
				return;
			}
		}
		else
		{
			return;
		}
	}
}
/**
 * Usuwa ze środka wielomianu jednomiany o współczynniku tożsamościowo równym zero.
 * @param[in] new_poly : wielomian
 */
static void InsideRemoving(Poly *new_poly)
{
	MonoList *before = new_poly->start; ///< wskaźnik na poprzedni element listy
	MonoList *list_iterator = new_poly->start; ///< iterator po elementach listy

	while (list_iterator != NULL)
	{
		if (PolyIsCoeff(&list_iterator->pointer->p))
		{
			if (list_iterator->pointer->p.c == 0)
			{
				MonoList *next = list_iterator->next;
				MonoListPop(&list_iterator);
				list_iterator = next;
				before->next = list_iterator;
			}
			else if (list_iterator->pointer->exp == 0)
			{
				new_poly->c = list_iterator->pointer->p.c;

				MonoList *next = list_iterator->next;
				MonoListPop(&list_iterator);
				list_iterator = next;
				before->next = list_iterator;
			}
			else
			{
				before = list_iterator;
				list_iterator = list_iterator->next;
			}
		}
		else
		{
			before = list_iterator;
			list_iterator = list_iterator->next;
		}
	}
}
/**
 * Usuwa z wielomianu jednomiany o współczynniku tożsamościowo równym zero.
 * @param[in] new_poly : wielomian
 */
static void RemoveUselessMonos(Poly *new_poly)
{
	FrontRemoving(new_poly);
	InsideRemoving(new_poly);
}

/**
 * Wykonuje prostą kopię wielomianu. Utworzony w ten sposób wielomian ma
 * ten sam wyraz wolny oraz wskaźnik na listę jednomianów.
 * @param[in] p : wielomian
 * @return prosta kopia wielomianu
 */
Poly QuickPolyCopy(const Poly *p)
{
	Poly new_poly = PolyFromCoeff(p->c);
	new_poly.start = p->start;

	RemoveUselessMonos(&new_poly);
	return new_poly;
}
/**
 * Wykonuje prostą kopię jednomianu do pamięci dynamicznej.
 * Nowy jednomian ma ten sam wykładnik oraz skopiowany w niegłęboki sposób
 * współczynnik
 * @param[in] m : jednomian
 * @return wskaźnik na jednomian zaalokowany w pamięci dynamicznej
 */
Mono *DynamicQuickMonoCopy(const Mono *m)
{
	Mono *new_mono = MONOALLOC;
	assert(new_mono != NULL);

	new_mono->exp = m->exp;
	new_mono->p = QuickPolyCopy(&(m->p));

	return new_mono;
}

/**
 * Sprawdza najmniejszy wykładnik jednomianu składającego sie na wielomian
 * i decyduje o umiejscowieniu wyrazu wolnego - jeżeli nie ma jednomianu 
 * o wykładniku zerowym to dodaje wyraz wolny na koniec wielomianu; 
 * w przeciwnym przypadku dodaje go do jednomianu o wykładniku równym zero.
 * @param[in] new_poly : wielomian
 * @param[in] constant : dodawany wyraz wolny
 */
static inline void CheckConstantAfterClone(Poly *new_poly, poly_coeff_t constant)
{
	MonoList *list_iterator = new_poly->start;

	while (list_iterator != NULL)
	{
		if (MonoExpIsZero(list_iterator) == NEGATIVE_TEN)
		{
			list_iterator->pointer->p.c += constant;
			return;
		}

		list_iterator = list_iterator->next;
	}

	new_poly->c += constant;
}
/**
 * Sprawdza przypadki brzegowe dodawania - dodawanie wielomianów tożsamościowo
 * równych zero lub współczynników.
 * Zwraca wielomian tożsamościowo równy zero, jeżeli dodaje się dwa wielomiany,
 * które nie są współczynnikami.
 * Jeżeli zachodzi przypadek brzegowy, to funkcja nadpisuje zmienną @p is_checked
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @param[in] p_exp : stopień wielomianu @p p ze względu na zmienną główną
 * @param[in] q_exp : stopień wielomianu @p q ze względu na zmienną główną
 * @param[in] is_checked : wskaźnik na zmienną logiczną
 * @result wielomian stworzony w przypadku brzegowym lub tożsamościowo równy zero
 */
static Poly CheckAddConstant(const Poly *p, const Poly *q, poly_exp_t p_exp, poly_exp_t q_exp, bool *is_checked)
{
	Poly new_poly = PolyZero();

	if (p_exp == -1 && q_exp == -1)
	{
		*is_checked = true;
		new_poly = PolyZero();
	}
	else if (p_exp == 0 && q_exp == 0)
	{
		*is_checked = true;
		new_poly = PolyFromCoeff(p->c + q->c);
	}
	else if (p_exp == -1 && q_exp != 0)
	{
		*is_checked = true;
		new_poly = PolyClone(q);
	}
	else if (p_exp == 0 && q_exp != 0)
	{
		*is_checked = true;
		new_poly = PolyClone(q);
		CheckConstantAfterClone(&new_poly, p->c);
	}
	else if (p_exp != 0 && q_exp == -1)
	{
		*is_checked = true;
		new_poly = PolyClone(p);
	}
	else if (p_exp != 0 && q_exp == 0)
	{
		*is_checked = true;
		new_poly = PolyClone(p);
		CheckConstantAfterClone(&new_poly, q->c);
	}

	return new_poly;
}
/**
 * Dodaje do nowopowstającego wielomianu pojedynczy jednomian.
 * Jest to przypadek, gdy jeden z wielomianów dodawanych do siebie ma składnik
 * o wykładniku niewystępującym w drugim.
 * Korzystanie z funkcji zakłada, że pierwszy wielomian (@p p) jest wielomianem
 * z wyjątkowym składnikiem.
 * @param[in] new_list : element listy jednomianów dla nowopowstającego wielomianu
 * @param[in] p_list_iterator : wskaźnik na element listy z jednomianem wielomianu @p p
 * @param[in] q_list_iterator : wskaźnik na element listy drugiego wielomianu
 * @param[in] constant : wyraz wolny nowotworzonego wielomianu
 * @param[in] p_exp : wykładnik rozpatrywanego jednomianu wielomianu @p p
 * @param[in] q_exp : wykładnik rozpatrywanego jednomianu drugiego wielomianu
 */
static void MonoListAppendElement(MonoList **new_list, MonoList **p_list_iterator, MonoList **q_list_iterator, 
	                              poly_coeff_t constant, poly_exp_t *p_exp, poly_exp_t *q_exp)
{
	if (*q_exp == 0)
	{
		(*new_list)->pointer = DynamicMonoClone((*p_list_iterator)->pointer);
		(*new_list)->pointer->p.c += constant;

		(*p_list_iterator) = (*p_list_iterator)->next;
		*p_exp = ListPointerIsNull(*p_list_iterator);
	}
	else
	{
		(*new_list)->pointer = DynamicMonoClone((*q_list_iterator)->pointer);

		(*q_list_iterator) = (*q_list_iterator)->next;
		*q_exp = ListPointerIsNull(*q_list_iterator);
	}
}
/**
 * Dodaje do nowopowstającego wielomianu sumę dwóch jednomianów.
 * Są to składniki wielomianu @p p i @p q.
 * @param[in] new_list : element listy jednomianów dla nowopowstającego wielomianu
 * @param[in] p_list_iterator : wskaźnik na element listy z jednomianem wielomianu @p p
 * @param[in] q_list_iterator : wskaźnik na element listy z jednomianem wielomianu @p q
 * @param[in] constant : wyraz wolny nowotworzonego wielomianu
 * @param[in] p_exp : wykładnik rozpatrywanego jednomianu wielomianu @p p
 * @param[in] q_exp : wykładnik rozpatrywanego jednomianu wielomianu @p q
 */
static void MonoListAddElements(MonoList **new_list, MonoList **p_list_iterator, MonoList **q_list_iterator,
	poly_coeff_t *constant, poly_exp_t *p_exp, poly_exp_t *q_exp)
{
	(*new_list)->pointer = MONOALLOC;
	assert((*new_list)->pointer != NULL);

	(*new_list)->pointer->exp = *p_exp;
	(*new_list)->pointer->p = PolyAdd(&(*p_list_iterator)->pointer->p, &(*q_list_iterator)->pointer->p);

	if (*p_exp == NEGATIVE_TEN)
	{
		(*new_list)->pointer->exp = 0;

		(*new_list)->pointer->p.c += *constant;
		*constant = 0;
	}

	*p_list_iterator = (*p_list_iterator)->next;
	*p_exp = ListPointerIsNull(*p_list_iterator);

	*q_list_iterator = (*q_list_iterator)->next;
	*q_exp = ListPointerIsNull(*q_list_iterator);
}
/**
* Dodaje dwa wielomiany.
* @param[in] p : wielomian
* @param[in] q : wielomian
* @return `p + q`
*/
Poly PolyAdd(const Poly *p, const Poly *q)
{
	Poly new_poly;
	poly_exp_t p_exp = MainPolyDeg(p);
	poly_exp_t q_exp = MainPolyDeg(q);
	bool is_checked = false; //zmienna logiczna przekazywana przez wskaźnik do CheckAddConstant

	new_poly = CheckAddConstant(p, q, p_exp, q_exp, &is_checked);

	if(!is_checked)
	{
		new_poly = PolyFromCoeff(p->c + q->c);
		MonoList *new_list = NULL;

		new_list = CreateListNode();
		new_poly.start = new_list;

		MonoList *p_list_iterator = p->start;
		MonoList *q_list_iterator = q->start;

		while (p_exp != 0 || q_exp != 0)
		{
			if (p_exp < q_exp)
			{
				MonoListAppendElement(&new_list, &p_list_iterator, &q_list_iterator, q->c, &p_exp, &q_exp);
			}
			else if (p_exp > q_exp)
			{
				MonoListAppendElement(&new_list, &q_list_iterator, &p_list_iterator, p->c, &q_exp, &p_exp);
			}
			else
			{
				MonoListAddElements(&new_list, &p_list_iterator, &q_list_iterator, &new_poly.c, &p_exp, &q_exp);
			}

			if (p_exp != 0 || q_exp != 0)
			{
				new_list->next = CreateListNode();
				new_list = new_list->next;
			}
		}

		RemoveUselessMonos(&new_poly);
	}
	return new_poly;
}

/**
 * Komparator porównujący dwa jednomiany po ich wykładnikach.
 * Sortuje jednomiany w kolejności malejącej względem wykładników.
 * Zaprojektowany dla funkcji QuickSort.
 * @param[in] a : jednomian
 * @param[in] b : jednomian
 */
int MonoComparator(const void *a, const void *b)
{
	Mono *mono_a = (Mono *)a;
	Mono *mono_b = (Mono *)b;
	if (mono_a->exp < mono_b->exp)
	{
		return 1;
	}
	else if (mono_a->exp == mono_b->exp)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}
/**
 * Konstruuje nowy wielomian sumując listę jednomianów.
 * @param[in] count : liczba jednomianów
 * @param[in] new_monos : tablica posortowanych jednomianów
 * @param[in] constant : wyraz wolny wielomianu
 * @param[in] constant_counter : liczba współczynników w tablicy
 * @param[in] deleted_counter : liczba usuniętych jednomianów z tablicy
 * @return wielomian
 */
Poly ConstructPolyFromMonos(unsigned count, Mono new_monos[], poly_coeff_t constant, unsigned constant_counter,
	                        unsigned deleted_counter)
{
	MonoList *new_list = NULL;
	Poly new_poly = PolyFromCoeff(constant);

	if (constant_counter != count)
	{
		new_list = CreateListNode();
		new_poly.start = new_list;
	}

	unsigned new_count = count - constant_counter - deleted_counter;
	unsigned mono_counter = 0;

	for (unsigned i = 0; i < count; i++)
	{
		if (new_monos[i].exp != NEGATIVE_TEN)
		{
			new_list->pointer = DynamicQuickMonoCopy(&new_monos[i]);

			if (mono_counter < new_count - 1)
			{
				new_list->next = CreateListNode();
				new_list = new_list->next;
			}

			mono_counter++;
		}
	}

	RemoveUselessMonos(&new_poly);
	return new_poly;
}
/**
 * Sumuje listę jednomianów i tworzy z nich wielomian.
 * Przejmuje na własność zawartość tablicy @p monos.
 * Kopiuje zawartość tablicy @p monos, sortuje ją, sumuje jednomiany o takich
 * samych wykładnikach i z tak przygotowanej tablicy tworzy nowy wielomian.
 * @param[in] count : liczba jednomianów
 * @param[in] monos : tablica jednomianów
 * @return wielomian będący sumą jednomianów
 */
Poly PolyAddMonos(unsigned count, const Mono monos[])
{
	unsigned constant_counter = 0;
	unsigned deleted_counter = 0;
	poly_coeff_t constant = 0;
	Mono *new_monos = (Mono *) malloc(count * sizeof(Mono));

	//sumowanie jednomianów będących współczynnikami do pojedynczej stałej
	for (unsigned i = 0; i < count; i++)
	{
		new_monos[i] = MonoFromPoly(&monos[i].p, monos[i].exp);
		if (new_monos[i].exp == 0 && PolyIsCoeff(&new_monos[i].p))
		{
			constant += new_monos[i].p.c;
			new_monos[i].exp = NEGATIVE_TEN;
			constant_counter++;
		}
	}

	qsort(new_monos, count, sizeof(Mono), MonoComparator);

	//sumowanie jednomianów o tych samych wykładników i "wyłączanie ich" z tablicy
	for (unsigned i = 0; i < count - 1; i++)
	{
		if (new_monos[i].exp != NEGATIVE_TEN)
		{
			if (new_monos[i].exp == new_monos[i + 1].exp)
			{
				Poly new_mono_poly = PolyAdd(&new_monos[i].p, &new_monos[i + 1].p);
				MonoDestroy(&new_monos[i]);
				PolyDestroy(&new_monos[i+1].p);

				new_monos[i + 1].p = new_mono_poly;
				new_monos[i].exp = NEGATIVE_TEN;

				deleted_counter++;
			}
		}
	}

	Poly new_poly = ConstructPolyFromMonos(count, new_monos, constant, constant_counter, deleted_counter);

	free(new_monos);
	return new_poly;
}

/**
 * Zwraca ilość jednomianów, z których składa się wielomian
 * @param[in] p : wielomian
 * @return liczba jednomianów
 */
static unsigned CountElements(const Poly *p)
{
	MonoList *list_iterator = p->start;
	unsigned result = 0;

	while (list_iterator != NULL)
	{
		result++;
		list_iterator = list_iterator->next;
	}

	return result;
}
/**
 * Dynamicznie alokuje jednomian `p * x^e`, kopiując zawartość struktury 
 * wskazywanej przez @p p.
 * @param[in] p : wielomian - współczynnik jednomianu
 * @param[in] e : wykładnik
 * @return wskaźnik na jednomian `p * x^e` w pamięci dynamicznej
 */
static Mono *DynamicMonoFromPoly(const Poly *p, poly_exp_t e)
{
	Mono *new_mono = MONOALLOC;
	assert(new_mono != NULL);

	new_mono->exp = e;
	new_mono->p = PolyClone(p);

	return new_mono;
}
/**
 * Tworzy wielomian poprzez wymnożenie wielomianu @p p przez stałą @p coeff.
 * @param[in] p : wielomian
 * @param[in] coeff : stała liczbowa
 * @return wielomian postaci coeff * p
 */
static Poly PolyMulByConstant(const Poly *p, const poly_coeff_t coeff)
{
	Poly new_poly = PolyFromCoeff(p->c * coeff);

	if (!PolyIsCoeff(p))
	{
		MonoList *list_iterator = p->start;
		MonoList *new_list = CreateListNode();
		new_poly.start = new_list;

		while (list_iterator != NULL)
		{
			Poly multiplied_poly = PolyMulByConstant(&list_iterator->pointer->p, coeff);

			new_list->pointer = DynamicMonoFromPoly(&multiplied_poly, list_iterator->pointer->exp);

			PolyDestroy(&multiplied_poly);

			if (list_iterator->next != NULL)
			{
				new_list->next = CreateListNode();
				new_list = new_list->next;
			}

			list_iterator = list_iterator->next;
		}
	}

	RemoveUselessMonos(&new_poly);
	return new_poly;
}
/**
 * Zwraca jednomian mający za współczynnik wielomian będącego współczynnikiem pewnego 
 * jednomianu, pomnożonego przez pewną stałą liczbową.
 * @param[in] list_iterator : element listy jednomianów
 * @param[in] constant : stała liczbowa
 * @return jednomian
 */
static inline Mono MonoFromPolyMulByConstant(const MonoList *list_iterator, const poly_coeff_t constant)
{
	Poly tmp_poly = PolyMulByConstant(&list_iterator->pointer->p, constant);
	return MonoFromPoly(&tmp_poly, list_iterator->pointer->exp);
}
/**
 * Zwraca jednomian mający za współczynnik wielomian powstały z pomnożenia dwóch
 * jednomianów.
 * Są to jednomiany składające się odpowiednio na wielomiany @p p i @p q.
 * @param[in] p_list_iterator : element listy jednomianów wielomianu @p p
 * @param[in] q_list_iterator : element listy jednomianów wielomianu @p q
 * @return jednomian
 */
static inline Mono MonoFromPolyMul(const MonoList *p_list_iterator, const MonoList *q_list_iterator)
{
	Poly tmp_poly = PolyMul(&p_list_iterator->pointer->p, &q_list_iterator->pointer->p);
	return MonoFromPoly(&tmp_poly, p_list_iterator->pointer->exp + q_list_iterator->pointer->exp);
}
/**
 * Sprawdza przypadki brzegowe mnożenia - mnożenie przez współczynnik.
 * Jeżeli zachodzi przypadek brzegowy, to funkcja nadpisuje zmienną @p is_checked
 * i zwraca wielomian. W przeciwnym przypadku zwraca wielomian tożsamościowo
 * równy zero.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @param[in] is_checked : wskaźnik na zmienną logiczną
 * @result wielomian stworzony w przypadku brzegowym lub tożsamościowo równy zero
 */
static Poly CheckMulByConstant(const Poly *p, const Poly *q, bool *is_checked)
{
	if (PolyIsCoeff(p) && PolyIsCoeff(q)) //stała * stała
	{
		*is_checked = true;
		return PolyFromCoeff(p->c * q->c);
	}
	else if (PolyIsCoeff(p)) //stała * wielomian
	{
		*is_checked = true;
		return PolyMulByConstant(q, p->c);
	}
	else if (PolyIsCoeff(q)) //wielomian * stała
	{
		*is_checked = true;
		return PolyMulByConstant(p, q->c);
	}

	return PolyZero();
}
/**
 * Mnoży dwa wielomiany.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p * q`
 */
Poly PolyMul(const Poly *p, const Poly *q)
{
	bool is_checked = false;
	Poly new_poly = CheckMulByConstant(p, q, &is_checked);

	if(!is_checked) //wielomian * wielomian
	{
		MonoList *p_list_iterator = p->start;
		MonoList *q_list_iterator = q->start;

		unsigned elements_count = (CountElements(p) + 1) * (CountElements(q) + 1); //+ 1, bo jeszcze stała
		Mono *new_elements = (Mono*) malloc(sizeof(Mono) * elements_count); ///< tablica nowych jednomianów
		unsigned i = 0;

		while (p_list_iterator != NULL)
		{
			while (q_list_iterator != NULL)
			{
				new_elements[i] = MonoFromPolyMul(p_list_iterator, q_list_iterator);
				q_list_iterator = q_list_iterator->next;
				i++;
			}
			if (q->c != 0)
			{
				new_elements[i] = MonoFromPolyMulByConstant(p_list_iterator, q->c);
				i++;
			}

			p_list_iterator = p_list_iterator->next;
			q_list_iterator = q->start;
		}

		if (p->c != 0)
		{
			q_list_iterator = q->start;
			while (q_list_iterator != NULL)
			{
				new_elements[i] = MonoFromPolyMulByConstant(q_list_iterator, p->c);
				i++;
				q_list_iterator = q_list_iterator->next;
			}
		}

		new_poly = PolyAddMonos(i, new_elements);
		new_poly.c = p->c * q->c;
		RemoveUselessMonos(&new_poly);

		free(new_elements);
	}

	return new_poly;
}

/**
 * Zwraca przeciwny wielomian.
 * @param[in] p : wielomian
 * @return `-p`
 */
Poly PolyNeg(const Poly *p)
{
	return PolyMulByConstant(p, (-1));
}

/**
 * Odejmuje wielomian od wielomianu.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p - q`
 */
Poly PolySub(const Poly *p, const Poly *q)
{
	Poly negative_q = PolyNeg(q);
	Poly result = PolyAdd(p, &negative_q);
	PolyDestroy(&negative_q);
	return result;
}

/**
 * Zwraca stopień wielomianu ze względu na zadaną zmienną (-1 dla wielomianu
 * tożsamościowo równego zeru).
 * Zmienne indeksowane są od 0.
 * Zmienna o indeksie 0 oznacza zmienną główną tego wielomianu.
 * Większe indeksy oznaczają zmienne wielomianów znajdujących się
 * we współczynnikach.
 * @param[in] p : wielomian
 * @param[in] var_idx : indeks zmiennej
 * @return stopień wielomianu @p p z względu na zmienną o indeksie @p var_idx
 */
poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx)
{
	if (var_idx == 0)
	{
		return MainPolyDeg(p);
	}
	else
	{
		MonoList *list_iterator = p->start;
		poly_exp_t result = -1;
		bool result_is_found = false;

		while (list_iterator != NULL)
		{
			poly_exp_t temp_result = PolyDegBy(&list_iterator->pointer->p, var_idx - 1);
			result = MAX(result, temp_result);
			result_is_found = true;

			list_iterator = list_iterator->next;
		}

		if (result_is_found)
		{
			result = MAX(result, 0);
		}
			return result;
	}
}

/**
 * Zwraca stopień wielomianu (-1 dla wielomianu tożsamościowo równego zeru).
 * @param[in] p : wielomian
 * @return stopień wielomianu @p p
 */
poly_exp_t PolyDeg(const Poly *p)
{
	if (PolyIsZero(p))
	{
		return (-1);
	}
	else if (PolyIsCoeff(p))
	{
		return 0;
	}
	else
	{
		poly_exp_t result = -1;
		MonoList *list_iterator = p->start;

		while (list_iterator != NULL)
		{
			poly_exp_t mono_result = PolyDeg(&list_iterator->pointer->p);

			if (mono_result == -1)
			{
				mono_result = 0;
			}
			else
			{
				mono_result += list_iterator->pointer->exp;
			}

			result = MAX(result, mono_result);

			list_iterator = list_iterator->next;
		}

		return result;
	}
}

/**
 * Sprawdza równość dwóch wielomianów.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p = q`
 */
bool PolyIsEq(const Poly *p, const Poly *q)
{
	if (p->c != q->c)
	{
		return false;
	}
	else
	{
		MonoList *p_list_iterator = p->start;
		MonoList *q_list_iterator = q->start;

		while (p_list_iterator != NULL)
		{
			if (q_list_iterator == NULL)
			{
				return false;
			}

			if (p_list_iterator->pointer->exp != q_list_iterator->pointer->exp)
			{
				return false;
			}
			else
			{
				if (PolyIsEq(&p_list_iterator->pointer->p, &q_list_iterator->pointer->p))
				{
					p_list_iterator = p_list_iterator->next;
					q_list_iterator = q_list_iterator->next;
				}
				else
				{
					return false;
				}
			}
		}
	}

	return true;
}

/**
 * Wylicza wartość potęgi `x^exp` dla @p x.
 * Korzysta z algorytmu szybkiego potęgowania.
 * @param[in] x : liczba
 * @param[in] exp : wykładnik potęgi
 * @return `x^exp`
 */
poly_coeff_t FastExponent(poly_coeff_t x, poly_exp_t exp)
{
	if (exp == 0)
	{
		return 1;
	}
	else if (exp % 2 != 0)
	{
		return x * FastExponent(x, exp - 1);
	}
	else
	{
		poly_coeff_t a = FastExponent(x, exp / 2);
		return a * a;
	}
}
/**
 * Wylicza wartość wielomianu w punkcie @p x.
 * Wstawia pod pierwszą zmienną wielomianu wartość @p x.
 * W wyniku może powstać wielomian, jeśli współczynniki są wielomianem
 * i zmniejszane są indeksy zmiennych w takim wielomianie o jeden.
 * Formalnie dla wielomianu @f$p(x_0, x_1, x_2, \ldots)@f$ wynikiem jest
 * wielomian @f$p(x, x_0, x_1, \ldots)@f$.
 * @param[in] p
 * @param[in] x
 * @return @f$p(x, x_0, x_1, \ldots)@f$
 */
Poly PolyAt(const Poly *p, poly_coeff_t x)
{
	Poly new_poly = PolyFromCoeff(p->c);

	if (x == 0 || PolyIsCoeff(p))
	{
		return new_poly;
	}
	else
	{
		MonoList *list_iterator = p->start;

		while (list_iterator != NULL)
		{
			poly_coeff_t evaluated_value = FastExponent(x, list_iterator->pointer->exp);
			Poly tmp_multiplied = PolyMulByConstant(&list_iterator->pointer->p, evaluated_value);

			Poly tmp_poly = PolyAdd(&new_poly, &tmp_multiplied);
			PolyDestroy(&tmp_multiplied);
			PolyDestroy(&new_poly);

			new_poly = tmp_poly;

			list_iterator = list_iterator->next;
		}

		RemoveUselessMonos(&new_poly);

		return new_poly;
	}
}

//Predeklaracja funkcji PolyFromMonoExponentation
Poly PolyFromMonoExponentation(const Mono *m, poly_exp_t i, unsigned count, const Poly x[]);

/**
 * Wylicza wartość wielomianu podniesionego do potęgi `p^exp`.
 * Korzysta z algorytmu szybkiego potęgowania.
 * @param[in] p : wielomian
 * @param[in] exp : wykładnik potęgi
 * @return `p^exp`
 */
Poly PolyFastExponent(const Poly *p, poly_exp_t exp)
{
	Poly to_return;
	Poly after_exponentation;

	if (exp == 0)
	{
		to_return = PolyFromCoeff(1);
	}
	else if (exp % 2 != 0)
	{
		after_exponentation = PolyFastExponent(p, exp - 1);
		to_return = PolyMul(p, &after_exponentation);
		PolyDestroy(&after_exponentation);
	}
	else
	{
		after_exponentation = PolyFastExponent(p, exp / 2);
		to_return = PolyMul(&after_exponentation, &after_exponentation);
		PolyDestroy(&after_exponentation);
	}

	return to_return;
}
/**
 * Zwraca wielomian stworzony z wielomianu @p , pod którego kolejne zmienne podstawiono
 * wielomiany.
 * Iteruje przez kolejne jednomiany składające się na wielomian i podstawia
 * pod ich zmienne kolejne wielomiany z tablicy @p x.
 * @param[in] p : wielomian
 * @param[in] i : indeks zmiennej
 * @param[in] count : ilość wielomianów w tablicy
 * @param[in] x : tablica wielomianów
 * @return wielomian
 */
Poly PolyComposeAndIterate(const Poly *p, poly_exp_t i, unsigned count, const Poly x[])
{
	Poly new_poly = PolyFromCoeff(p->c);
	MonoList *list_iterator = p->start;

	while (list_iterator != NULL)
	{
		Poly poly_from_exponentation = PolyFromMonoExponentation(list_iterator->pointer, i, count, x);
		Poly tmp_poly = PolyAdd(&poly_from_exponentation, &new_poly);

		PolyDestroy(&new_poly);
		PolyDestroy(&poly_from_exponentation);

		new_poly = tmp_poly;

		list_iterator = list_iterator->next;
	}

	return new_poly;
}
/**
 * Zwraca wielomian utworzony z podstawienia wielomianu @p x[i] pod zmienną
 * jednomianu @p m.
 * @param[in] m : jednomian
 * @param[in] i : indeks zmiennej
 * @param[in] count : ilość wielomianów w tablicy
 * @param[in] x : tablica wielomianów
 * @return wielomian
 */
Poly PolyFromMonoExponentation(const Mono *m, poly_exp_t i, unsigned count, const Poly x[])
{
	Poly new_coeff;
	Poly new_poly_from_exponentation;
	Poly new_poly_to_return;

	if ((unsigned)i >= count)
	{
		new_poly_to_return = PolyZero();
		return new_poly_to_return;
	}
	else
	{
		new_coeff = PolyComposeAndIterate(&m->p, i + 1, count, x);
		new_poly_from_exponentation = PolyFastExponent(&x[i], m->exp);
		new_poly_to_return = PolyMul(&new_coeff, &new_poly_from_exponentation);

		PolyDestroy(&new_coeff);
		PolyDestroy(&new_poly_from_exponentation);
	}

	return new_poly_to_return;
}
/**
 * Zwraca wielomian @p p, w którym pod zmienną @p x_i podstawiany jest
 * wielomian @p x[i] z zadanej tablicy.
 * Jeżeli wielomianów w tablicy będzie istotnie mniej niż zmiennych w wielomianie,
 * funkcja podstawi pod te zmienne wielomiany tożsamościowo równe zeru.
 * @param[in] p : wielomian
 * @param[in] count : ilość wielomianów w tablicy
 * @param[in] x : tablica wielomianów
 * @return wielomian
*/Poly PolyCompose(const Poly *p, unsigned count, const Poly x[])
{
	return PolyComposeAndIterate(p, 0, count, x);
}

/**
 * Zwraca wskaźnik na początek listy jednomianów danego wielomianu @p p.
 * @param[in] p : wielomian
 * @return wskaźnik na początek listy jednomianów
 */
MonoList *ReturnMonoListStart(const Poly *p)
{
	return p->start;
}

/**
 * Sprawdza, czy lista jednomianów w danym miejscu już się skończyła.
 * Służy do iteracji po liście monomianów
 * @param[in] list_iterator : wskaźnik na element listy jednomianów
 * @return Czy lista się skończyła?
 */
bool MonoListIsEmpty(const MonoList *list_iterator)
{
	return (list_iterator == NULL);
}

/**
 * Iteruje po liście jednomianów.
 * @param[in] list_iterator : wskaźnik na aktualny element listy
 */
void IterateMonoList(MonoList **list_iterator)
{
	(*list_iterator) = (*list_iterator)->next;
}

/**
 * Zwraca wskaźnik na jednomian trzymany na liście jednomianów,
 * dla zadanego wskaźnika na element listy.
 * @param[in] list_iterator : element listy
 * @return wskaźnik na jednomian
 */
Mono *ReturnMono(MonoList *list_iterator)
{
	return list_iterator->pointer;
}

/**
 * Zwraca wyraz wolny wielomianu @p p
 * @param[in] p : wielomian
 * @return wyraz wolny wielomianu
 */
poly_coeff_t ReturnPolyCoeff(const Poly *p)
{
	return p->c;
}