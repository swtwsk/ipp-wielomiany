/** @file
   Interfejs klasy wielomianów

   @author Jakub Pawlewicz <pan@mimuw.edu.pl>, Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-06-01
*/

#ifndef __POLY_H__
#define __POLY_H__

#include <stdbool.h>
#include <stddef.h>

/** Dynamiczna alokacja struktury przechowującej wielomian. */
#define POLYALLOC (Poly *) malloc(sizeof(Poly))
/** Dynamiczna alokacja struktury przechowującej jednomian. */
#define MONOALLOC (Mono *) malloc(sizeof(Mono))
/** Dynamiczna alokacja struktury przechowującej listę jednomianów. */
#define LISTALLOC (MonoList *) malloc(sizeof(MonoList))

/** Typ współczynników wielomianu */
typedef long poly_coeff_t;

/** Typ wykładników wielomianu */
typedef int poly_exp_t;

/** Struktura przechowująca listę jednomianów */
typedef struct MonoList MonoList;

/**
 * Struktura przechowująca wielomian
 * Wielomian jest sumą jednomianów oraz wyrazu wolnego.
 * Posortowane po wykładnikach w kolejności malejącej
 * jednomiany są umieszczone na liscie.
 */
typedef struct Poly
{
	struct MonoList *start; ///< wskaźnik na listę jednomianów
	poly_coeff_t c; ///< wyraz wolny wielomianu
} Poly;

/**
  * Struktura przechowująca jednomian
  * Jednomian ma postać `p * x^e`.
  * Współczynnik `p` może też być wielomianem.
  * Będzie on traktowany jako wielomian nad kolejną zmienną (nie nad x).
  */
typedef struct Mono
{
    struct Poly p; ///< współczynnik
    poly_exp_t exp; ///< wykładnik
} Mono;

/**
 * Tworzy wielomian, który jest współczynnikiem.
 * @param[in] c : wartość współczynnika
 * @return wielomian
 */
static inline Poly PolyFromCoeff(poly_coeff_t c)
{
	return (Poly) { .start = NULL, .c = c };
}

/**
 * Tworzy wielomian tożsamościowo równy zeru.
 * @return wielomian
 */
static inline Poly PolyZero()
{
	return (Poly) { .start = NULL, .c = 0 };
}

/**
 * Tworzy jednomian `p * x^e`.
 * Przejmuje na własność zawartość struktury wskazywanej przez @p p.
 * @param[in] p : wielomian - współczynnik jednomianu
 * @param[in] e : wykładnik
 * @return jednomian `p * x^e`
 */
static inline Mono MonoFromPoly(const Poly *p, poly_exp_t e)
{
    return (Mono) {.p = *p, .exp = e};
}

/**
 * Sprawdza, czy wielomian jest współczynnikiem.
 * @param[in] p : wielomian
 * @return Czy wielomian jest współczynnikiem?
 */
static inline bool PolyIsCoeff(const Poly *p)
{
	return (p->start == NULL);
}

/**
 * Sprawdza, czy wielomian jest tożsamościowo równy zeru.
 * @param[in] p : wielomian
 * @return Czy wielomian jest równy zero?
 */
static inline bool PolyIsZero(const Poly *p)
{
	if (PolyIsCoeff(p))
	{
		return (p->c == 0);
	}
	else
	{
		return false;
	}
}

/**
 * Usuwa wielomian z pamięci.
 * @param[in] p : wielomian
 */
void PolyDestroy(Poly *p);

/**
 * Usuwa jednomian z pamięci.
 * @param[in] m : jednomian
 */
static inline void MonoDestroy(Mono *m)
{
	PolyDestroy(&(m->p));
}

/**
 * Robi pełną, głęboką kopię wielomianu.
 * @param[in] p : wielomian
 * @return skopiowany wielomian
 */
Poly PolyClone(const Poly *p);

/**
 * Robi pełną, głęboką kopię jednomianu.
 * @param[in] m : jednomian
 * @return skopiowany jednomian
 */
static inline Mono MonoClone(const Mono *m)
{
	return (Mono) { .p = PolyClone(&(m->p)), .exp = m->exp };
}

/**
 * Dodaje dwa wielomiany.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p + q`
 */
Poly PolyAdd(const Poly *p, const Poly *q);

/**
 * Sumuje listę jednomianów i tworzy z nich wielomian.
 * Przejmuje na własność zawartość tablicy @p monos.
 * @param[in] count : liczba jednomianów
 * @param[in] monos : tablica jednomianów
 * @return wielomian będący sumą jednomianów
 */
Poly PolyAddMonos(unsigned count, const Mono monos[]);

/**
 * Mnoży dwa wielomiany.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p * q`
 */
Poly PolyMul(const Poly *p, const Poly *q);

/**
 * Zwraca przeciwny wielomian.
 * @param[in] p : wielomian
 * @return `-p`
 */
Poly PolyNeg(const Poly *p);

/**
 * Odejmuje wielomian od wielomianu.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p - q`
 */
Poly PolySub(const Poly *p, const Poly *q);

/**
 * Zwraca stopień wielomianu ze względu na zadaną zmienną (-1 dla wielomianu
 * tożsamościowo równego zeru).
 * Zmienne indeksowane są od 0.
 * Zmienna o indeksie 0 oznacza zmienną główną tego wielomianu.
 * Większe indeksy oznaczają zmienne wielomianów znajdujących się
 * we współczynnikach.
 * @param[in] p : wielomian
 * @param[in] var_idx : indeks zmiennej
 * @return stopień wielomianu @p p z względu na zmienną o indeksie @p var_idx
 */
poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx);

/**
 * Zwraca stopień wielomianu (-1 dla wielomianu tożsamościowo równego zeru).
 * @param[in] p : wielomian
 * @return stopień wielomianu @p p
 */
poly_exp_t PolyDeg(const Poly *p);

/**
 * Sprawdza równość dwóch wielomianów.
 * @param[in] p : wielomian
 * @param[in] q : wielomian
 * @return `p = q`
 */
bool PolyIsEq(const Poly *p, const Poly *q);

/**
 * Wylicza wartość wielomianu w punkcie @p x.
 * Wstawia pod pierwszą zmienną wielomianu wartość @p x.
 * W wyniku może powstać wielomian, jeśli współczynniki są wielomianem
 * i zmniejszane są indeksy zmiennych w takim wielomianie o jeden.
 * Formalnie dla wielomianu @f$p(x_0, x_1, x_2, \ldots)@f$ wynikiem jest
 * wielomian @f$p(x, x_0, x_1, \ldots)@f$.
 * @param[in] p
 * @param[in] x
 * @return @f$p(x, x_0, x_1, \ldots)@f$
 */
Poly PolyAt(const Poly *p, poly_coeff_t x);

/**
 * Zwraca wielomian @p p, w którym pod zmienną @p x_i podstawiany jest
 * wielomian @p x[i] z zadanej tablicy.
 * Jeżeli wielomianów w tablicy będzie istotnie mniej niż zmiennych w wielomianie,
 * funkcja podstawi pod te zmienne wielomiany tożsamościowo równe zeru.
 * @param[in] p : wielomian
 * @param[in] count : ilość wielomianów w tablicy
 * @param[in] x : tablica wielomianów
 * @return wielomian
 */
Poly PolyCompose(const Poly *p, unsigned count, const Poly x[]);

/**
* Zwraca wskaźnik na początek listy jednomianów danego wielomianu @p p.
* @param[in] p : wielomian
* @return wskaźnik na początek listy jednomianów
*/
MonoList *ReturnMonoListStart(const Poly *p);

/**
* Sprawdza, czy lista jednomianów w danym miejscu już się skończyła.
* Służy do iteracji po liście monomianów
* @param[in] list_iterator : wskaźnik na element listy jednomianów
* @return Czy lista się skończyła?
*/
bool MonoListIsEmpty(const MonoList *list_iterator);

/**
* Iteruje po liście jednomianów.
* @param[in] list_iterator : wskaźnik na aktualny element listy
*/
void IterateMonoList(MonoList **list_iterator);

/**
* Zwraca wskaźnik na jednomian trzymany na liście jednomianów,
* dla zadanego wskaźnika na element listy.
* @param[in] list_iterator : element listy
* @return wskaźnik na jednomian
*/
Mono *ReturnMono(MonoList *list_iterator);

/**
* Zwraca wyraz wolny wielomianu @p p
* @param[in] p : wielomian
* @return wyraz wolny wielomianu
*/
poly_coeff_t ReturnPolyCoeff(const Poly *p);

#endif /* __POLY_H__ */
