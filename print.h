/** @file
   Obsługa wyświetlania wielomianów

   @author Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @date 2017-05-24
*/

#ifndef __PRINT_H__
#define __PRINT_H__

#include <stdbool.h>
#include "poly.h"

/**
* Wyświetla wielomian @p poly_to_print na standardowe wyjście.
* Wielomian uporządkowany jest w kolejności rosnącej względem wielkości
* wykładnika.
* @param[in] poly_to_print : wielomian
*/
void PolyPrint(Poly *poly_to_print);

#endif
