/** @file
   Testy jednostkowe kalkulatora podstawowych operacji na wielomianach

   @author Tomasz Kociumaka, IPP team, Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @copyright Google Inc., Tomasz Kociumaka, Uniwersytet Warszawski
   @date 2017-06-05
*/

#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "cmocka.h"
#include "poly.h"

/** Atrapa funkcji main kalkulatora */
extern int calc_main();

/* Funkcje przygotowane przez autorów cmocka */
/**
 * Pomocnicze bufory, do których piszą atrapy funkcji printf i fprintf oraz
 * pozycje zapisu w tych buforach. Pozycja zapisu wskazuje bajt o wartości 0.
 */
static char fprintf_buffer[256]; ///< bufor pomocniczy atrapy funkcji fprintf
static char printf_buffer[256]; ///< bufor pomocniczy atrapy funkcji printf
static int fprintf_position = 0; ///< pozycja zapisu w buforze pomocniczym atrapy fprintf
static int printf_position = 0; ///< pozycja zapisu w buforze pomocniczym atrapy printf

/**
 * Atrapa funkcji fprintf sprawdzająca poprawność wypisywania na stderr.
 */
int mock_fprintf(FILE* const file, const char *format, ...) {
    int return_value;
    va_list args;

    assert_true(file == stderr);
    /* Poniższa asercja sprawdza też, czy fprintf_position jest nieujemne.
    W buforze musi zmieścić się kończący bajt o wartości 0. */
    assert_true((size_t)fprintf_position < sizeof(fprintf_buffer));

    va_start(args, format);
    return_value = vsnprintf(fprintf_buffer + fprintf_position,
                             sizeof(fprintf_buffer) - fprintf_position,
                             format,
                             args);
    va_end(args);

    fprintf_position += return_value;
    assert_true((size_t)fprintf_position < sizeof(fprintf_buffer));
    return return_value;
}

/**
 * Atrapa funkcji fprintf sprawdzająca poprawność wypisywania na stdout.
 */
int mock_printf(const char *format, ...) {
    int return_value;
    va_list args;

    /* Poniższa asercja sprawdza też, czy printf_position jest nieujemne.
    W buforze musi zmieścić się kończący bajt o wartości 0. */
    assert_true((size_t)printf_position < sizeof(printf_buffer));

    va_start(args, format);
    return_value = vsnprintf(printf_buffer + printf_position,
                             sizeof(printf_buffer) - printf_position,
                             format,
                             args);
    va_end(args);

    printf_position += return_value;
    assert_true((size_t)printf_position < sizeof(printf_buffer));
    return return_value;
}

/**
 *  Pomocniczy bufor, z którego korzystają atrapy funkcji operujących na stdin.
 */
static char input_stream_buffer[256]; ///< pomocniczy bufor atrapy funkcji z stdin
static int input_stream_position = 0; ///< pozycja zapisu w buforze pomocniczym atrapy z stdin
static int input_stream_end = 0; ///< pozycja końca w buforze pomocniczym atrapy z stdin
int read_char_count; ///< 

/**
 * Atrapa funkcji scanf używana do przechwycenia czytania z stdin.
 */
int mock_scanf(const char *format, ...) {
    va_list fmt_args;
    int ret;

    va_start(fmt_args, format);
    ret = vsscanf(input_stream_buffer + input_stream_position, format, fmt_args);
    va_end(fmt_args);

    if (ret < 0) { /* ret == EOF */
        input_stream_position = input_stream_end;
    }
    else {
        assert_true(read_char_count >= 0);
        input_stream_position += read_char_count;
        if (input_stream_position > input_stream_end) {
            input_stream_position = input_stream_end;
        }
    }
    return ret;
}

/**
 * Atrapa funkcji getchar używana do przechwycenia czytania z stdin.
 */
int mock_getchar() {
    if (input_stream_position < input_stream_end)
        return input_stream_buffer[input_stream_position++];
    else
        return EOF;
}

/**
 * Atrapa funkcji ungetc.
 * Obsługiwane jest tylko standardowe wejście.
 */
int mock_ungetc(int c, FILE *stream) {
    assert_true(stream == stdin);
    if (input_stream_position > 0)
        return input_stream_buffer[--input_stream_position] = c;
    else
        return EOF;
}

/**
 * Funkcja wołana przed każdym testem.
 */
static int test_setup(void **state) {
    (void)state;

    memset(fprintf_buffer, 0, sizeof(fprintf_buffer));
    memset(printf_buffer, 0, sizeof(printf_buffer));
    printf_position = 0;
    fprintf_position = 0;

    /* Zwrócenie zera oznacza sukces. */
    return 0;
}

/**
 * Funkcja inicjująca dane wejściowe dla programu korzystającego ze stdin.
 */
static void init_input_stream(const char *str) {
    memset(input_stream_buffer, 0, sizeof(input_stream_buffer));
    input_stream_position = 0;
    input_stream_end = strlen(str);
    assert_true((size_t)input_stream_end < sizeof(input_stream_buffer));
    strcpy(input_stream_buffer, str);
}

/* Testy funkcji PolyCompose z pliku poly.c */
/**
 * Test wielomianu zerowego przy @p count równym 0
 */
static void test_zero_0(void **state)
{
    (void)state;

    Poly x[1];
    Poly zero = PolyZero();
    Poly res = PolyCompose(&zero, 0, x);

    assert_true(PolyIsEq(&res, &zero));
}
/**
 * Test wielomianu zerowego przy @p count równym 1, podstawiający wielomian stały
 */
static void test_zero_1(void **state)
{
    (void)state;

    Poly x[] = { PolyFromCoeff(2) };
    Poly zero = PolyZero();
    Poly res = PolyCompose(&zero, 1, x);

    assert_true(PolyIsEq(&res, &zero));
}
/**
 * Test wielomianu stałego przy @p count równym 0
 */
static void test_coeff_0(void **state)
{
    (void)state;

    Poly x[1];
    Poly coeff = PolyFromCoeff(2);
    Poly res = PolyCompose(&coeff, 0, x);

    assert_true(PolyIsEq(&res, &coeff));
}
/**
 * Test wielomianu stałego @p p przy @p count równym 1, podstawiający wielomian
 * stały różny od @p p
 */
static void test_coeff_1(void **state)
{
    (void)state;

    Poly x[] = { PolyFromCoeff(42) };
    Poly coeff = PolyFromCoeff(2);
    Poly res = PolyCompose(&coeff, 1, x);

    assert_true(PolyIsEq(&res, &coeff));
}

/**
 * Tworzy wielomian postaci @p p = x.
 * @return wielomian `x`
 */
static Poly AffinePoly()
{
    Mono *m = malloc(sizeof(Mono));
    Poly coeff = PolyFromCoeff(1);
    *m = MonoFromPoly(&coeff, 1);
    Poly p = PolyAddMonos(1, m);

    free(m);
    return p;
}
/**
 * Test wielomianu równego x przy @p count równym 0
 */
static void test_poly_0(void **state)
{
    (void)state;

    Poly x[1];
    Poly p = AffinePoly();

    Poly res = PolyCompose(&p, 0, x);

    assert_true(PolyIsEq(&res, &p));

    PolyDestroy(&p);
    PolyDestroy(&res);
}
/**
 * Test wielomianu równego x przy @p count równym 1, podstawiający wielomian stały
 */
static void test_poly_with_coeff(void **state)
{
    (void)state;

    Poly x[] = { PolyFromCoeff(2) };
    Poly p = AffinePoly();
    Poly res = PolyCompose(&p, 1, x);

    assert_true(PolyIsEq(&res, &x[0]));

    PolyDestroy(&p);
    PolyDestroy(&res);
    PolyDestroy(&x[0]);
}
/**
 * Test wielomianu równego x przy @p count równym 1, podstawiający wielomian
 * postaci `x`
 */
static void test_poly_with_poly(void **state)
{
    (void)state;

    Poly x[] = { AffinePoly() };
    Poly p = AffinePoly();
    Poly res = PolyCompose(&p, 1, x);

    assert_true(PolyIsEq(&res, &x[0]));
    
    PolyDestroy(&p);
    PolyDestroy(&res);
    PolyDestroy(&x[0]);
}

/* Testy parsowania polecenia COMPOSE w pliku calc.c */
/**
 * Test parsujący polecenie COMPOSE bez żadnego parametru
 */
static void test_parse_no_param(void **state)
{
    (void)state;

    init_input_stream("COMPOSE\n");
    
    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem równym 0
 */
static void test_parse_count_zero(void **state)
{
    (void)state;

    init_input_stream("COMPOSE 0");

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem równym UINT_MAX
 * UINT_MAX jest to największa wartość reprezentowana w typie unsigned
 */
static void test_parse_unsigned_max_val(void **state)
{
    (void)state;

    char str[20];
    sprintf(str, "%u", UINT_MAX);
    char res[30] = "COMPOSE ";
    strcat(res, str);
    char *end = "\n";
    strcat(res, end);
    init_input_stream(res);

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 STACK UNDERFLOW\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem równym -1
 */
static void test_parse_under_zero(void **state)
{
    (void)state;

    init_input_stream("COMPOSE -1");

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem równym UINT_MAX + 1
 */
static void test_parse_above_unsigned_max_val(void **state)
{
    (void)state;

    char str[20];
    sprintf(str, "%lu", (unsigned long)UINT_MAX + 1);
    char res[30] = "COMPOSE ";
    strcat(res, str);
    char *end = "\n";
    strcat(res, end);
    init_input_stream(res);

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem o wiele większym niż UINT_MAX
 * Na poczet testu wybrana została wartość ULONG_MAX (18,446,744,073,709,551,616)
 */
static void test_parse_great_above_unsigned_max_val(void **state)
{
    (void)state;

    char str[30];
    sprintf(str, "%lu", ULONG_MAX);
    char res[30] = "COMPOSE ";
    strcat(res, str);
    char *end = "\n";
    strcat(res, end);
    init_input_stream(res);

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem złożonym z samych liter
 */
static void test_parse_letters(void **state)
{
    (void)state;

    init_input_stream("COMPOSE SEEKANDDESTROY");

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/**
 * Test parsujący polecenie COMPOSE z parametrem złożonym z liczb oraz liter
 */
static void test_parse_digits_and_letters(void **state)
{
    (void)state;

    init_input_stream("COMPOSE 420TEST");

    assert_int_equal(calc_main(), 0);

    assert_string_equal(printf_buffer, "");
    assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}

/**
 * Główna funkcja programu testującego za pomocą testów jednostkowych
 * Do testowania używana jest biblioteka @p cmocka
 */
int main()
{
    const struct CMUnitTest poly_tests[] =
    {
        cmocka_unit_test(test_zero_0),
        cmocka_unit_test(test_zero_1),
        cmocka_unit_test(test_coeff_0),
        cmocka_unit_test(test_coeff_1),
        cmocka_unit_test(test_poly_0),
        cmocka_unit_test(test_poly_with_coeff),
        cmocka_unit_test(test_poly_with_poly),
    };

    const struct CMUnitTest parse_tests[] =
    {
        cmocka_unit_test_setup(test_parse_no_param, test_setup),
        cmocka_unit_test_setup(test_parse_count_zero, test_setup),
        cmocka_unit_test_setup(test_parse_unsigned_max_val, test_setup),
        cmocka_unit_test_setup(test_parse_under_zero, test_setup),
        cmocka_unit_test_setup(test_parse_above_unsigned_max_val, test_setup),
        cmocka_unit_test_setup(test_parse_great_above_unsigned_max_val, test_setup),
        cmocka_unit_test_setup(test_parse_letters, test_setup),
        cmocka_unit_test_setup(test_parse_digits_and_letters, test_setup)
    };

    return (cmocka_run_group_tests(poly_tests, NULL, NULL) || cmocka_run_group_tests(parse_tests, NULL, NULL));
}
