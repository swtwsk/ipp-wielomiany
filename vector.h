/** @file
   Interfejs wektora (samorozszerzającej się tablicy) jednomianów

   @author Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @date 2017-05-23
*/

#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stdbool.h>
#include "poly.h"

/** Kontener (samorozszerzająca się tablica) przechowujący jednomiany */
typedef struct Vector Vector;

/**
* Tworzy nowy, pusty kontener, o wielkości wyrażonej w stałej NEW_VECTOR_CAPACITY.
* @return nowy kontener
*/
Vector *CreateNewVector();

/**
* Dodaje nowy jednomian do kontenera.
* param[in] v : wskaźnik na kontener
* param[in] mono_to_add : jednomian
*/
void VectorAdd(Vector **v, Mono *mono_to_add);

/**
* Usuwa cały kontener z pamięci.
* @param[in] v : kontener
*/
void VectorDelete(Vector **v);

/**
* Zwraca ilość elementów (jednomianów) trzymanych wewnątrz kontenera @p v
* @param[in] v : kontener
* @return ilość elementów
*/
poly_exp_t VectorActiveSize(Vector *v);

/**
* Zwraca tablicę jednomianów trzymaną wewnątrz kontenera @p v
* @param[in] v : kontener
* @return wskaźnik na tablicę jednomianów
*/
Mono *VectorGetMonoTab(Vector *v);

#endif
