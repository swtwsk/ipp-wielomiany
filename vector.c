/** @file
   Interfejs wektora (samorozszerzającej się tablicy) jednomianów

   @author Andrzej Swatowski <as386085@students.mimuw.edu.pl>
   @date 2017-05-23
*/

#include "vector.h"
#include <stdlib.h>
#include <assert.h>

/** Dynamiczna alokacja kontenera przechowującego jednomiany */
#define VECTORALLOC (Vector *) malloc(sizeof(Vector) * NEW_VECTOR_CAPACITY)
/** Stała wyrażająca początkową wielkość kontenera */
#define NEW_VECTOR_CAPACITY 1

/** Kontener (samorozszerzająca się tablica) przechowujący jednomiany */
struct Vector {
	Mono *mono_tab; ///< wskaźnik na dynamicznie alokowaną tablicę jednomianów
	poly_exp_t size; ///< wielkość dynamicznie alokowanej tablicy
	poly_exp_t act_size; ///< ilość przechowywanych w tablicy elementów
};

/**
 * Tworzy nowy, pusty kontener, o wielkości wyrażonej w stałej NEW_VECTOR_CAPACITY.
 * @return nowy kontener
 */
Vector *CreateNewVector()
{
	Vector *new_vector = VECTORALLOC;
	assert(new_vector != NULL);

	new_vector->mono_tab = (Mono *) malloc(sizeof(Mono) * NEW_VECTOR_CAPACITY);
	assert(new_vector->mono_tab != NULL);

	new_vector->size = NEW_VECTOR_CAPACITY;
	new_vector->act_size = 0;

	return new_vector;
}

/**
 * Zmienia rozmiar tablicy, realokując pamięć, którą zajmuje.
 * @param[in] v : wskaźnik na kontener
 * @param[in] new_size : nowy rozmiar tablicy
 */
static void VectorResize(Vector **v, poly_exp_t new_size)
{
	Mono *new_mono_tab = (Mono *) realloc((*v)->mono_tab, new_size * sizeof(Mono));
	assert(new_mono_tab != NULL);

	(*v)->mono_tab = new_mono_tab;
	(*v)->size = new_size;
}

/**
 * Dodaje nowy jednomian do kontenera.
 * param[in] v : wskaźnik na kontener
 * param[in] mono_to_add : jednomian
 */
void VectorAdd(Vector **v, Mono *mono_to_add)
{
	//gdy nie starcza miejsca, kontener podwaja wielkość tablicy
	if ((*v)->act_size == (*v)->size)
	{
		VectorResize(v, 2 * (*v)->size);
	}

	(*v)->mono_tab[(*v)->act_size] = *mono_to_add;
	(*v)->act_size++;
}

/**
 * Usuwa cały kontener z pamięci.
 * @param[in] v : kontener
 */
void VectorDelete(Vector **v)
{
	free((*v)->mono_tab);
	free(*v);
}

/**
 * Zwraca ilość elementów (jednomianów) trzymanych wewnątrz kontenera @p v
 * @param[in] v : kontener
 * @return ilość elementów
 */
poly_exp_t VectorActiveSize(Vector *v)
{
	return v->act_size;
}

/**
 * Zwraca tablicę jednomianów trzymaną wewnątrz kontenera @p v
 * @param[in] v : kontener
 * @return wskaźnik na tablicę jednomianów
 */
Mono *VectorGetMonoTab(Vector *v)
{
	return v->mono_tab;
}